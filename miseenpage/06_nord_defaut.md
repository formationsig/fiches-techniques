![](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![](images/CC.png) 

[Inrap](https://www.inrap.fr/), réseau des référents SIG - Bertrand Houdusse (2024)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.34 de QGIS
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).





# Remplacer le nord par défaut dans une mise en page



## Trouver le nord qui convient

Le symbole du nord dans Qgis est un fichier SVG. Par défaut, Qgis en propose plusieurs, qui sont stockés dans un sous-dossier SVG du dossier d'installation de Qgis (par exemple : `C:\Program Files\QGIS\apps\qgis\svg`)

Si l'on souhaite en utiliser un autre nord par défaut, il est recommandé, une fois le fichier déterminé, de l'enregistrer soit dans ce sous-dossier, soit dans un autre dossier où l'utilisateur peut placer ses différentes ressources liées à Qgis (par exemple dans Documents\SIG)



## Copier le chemin complet vers le fichier

Une fois le fichier SVG enregistré à l'emplacement souhaité, il faut obtenir son chemin (= emplacement de stockage) complet.

Pour ce faire, il faut ouvrir les propriétés du fichier (clic droit > propriétés), puis sélectionner le texte affiché en haut de l'onglet Sécurité, en face de "Nom de l'objet", et le copier (capture écran d'écran sous Win10)

![chemin_fichier](images/06_nord_defaut/image-20240703163322572.png)



## Régler le nord par défaut dans Qgis

Dans Qgis, il faut se rendre dans les préférences du logiciel, dans les réglages avancés.

Menu Préférences > Options

À gauche, dans la barre d'onglet, cliquer sur l'onglet tout en bas : Avancé

Dans la fenêtre à droite, il faut

- décocher "Use new settings tree widget" (sinon, le paramètre recherché ne sera pas visible ; valable sur Qgis 3.34)
- cliquer sur le bouton "'Je ferai attention, je le promets"



![pref_avancees](images/06_nord_defaut/image-20240703163828870.png)





- Une fois la fenêtre des réglages avancés ouverte, il faut utiliser la barre de recherche en haut à gauche de la fenêtre d'options, et saisir "north" ; l'affichage sera filtré sur les valeurs contenant ce texte

![pref_avance_nord](images/06_nord_defaut/image-20240703164219981.png)

- Il suffit ensuite de remplacer (en double-cliquant sur la valeur existante) la valeur inscrite en face de "QString" par le chemin du fichier SVG de votre nord favori

![pref_nord_valeur](images/06_nord_defaut/image-20240703164501646.png)

- Quitter la fenêtre des Options en cliquant sur le bouton OK



## Ajouter un nord dans une mise en page

Dans le projet en cours, ouvrir une nouvelle mise en page (Ctrl+P), et ajouter un nord en cliquant sur le bouton "Ajouter Flèche du Nord" ![nord_bouton](images/06_nord_defaut/image-20240703164905906.png)

Le nord affiché correspond normalement au nouveau nord défini par défaut.


------

Source : [https://gis.stackexchange.com/questions/380880/editing-and-changing-the-default-symbol-of-north-arrow-in-qgis-3-10](https://gis.stackexchange.com/questions/380880/editing-and-changing-the-default-symbol-of-north-arrow-in-qgis-3-10)