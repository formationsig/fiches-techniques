![logo inrap](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![](images/CC.png)   

[Inrap](https://www.inrap.fr/) - équipe Formateurs & Référents SIG (2021)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS. 
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).



# Mise en page: Créer une carte simple

L'outil de mise en page est accessible:

* par l'icône ![icone nouvelle mise en page](images/mep_01.png)dans la barre d'outils *Projet*.
* *via* le Menu *Projet* → *Nouvelle mise en page*
* avec le raccourci [Ctrl] + [P]

:information_source: Avant la version  3 de QGIS, l'outil appelait "composeur d'impression" .



![mise en page base](images/mep_02.png)	

:information_source: :warning: Pour spécifier la taille de la page : clic droit dans la zone de rendu de la mise en page.



Ne pas hésiter à rafraîchir avec ![icone rafraîchir](images/mep_03.png) 



## Ajouter une carte

Sélectionner l'outil ![icone carte](images/mep_04.png)ou *Ajouter un élément* → *Ajouter Carte* et tracer un rectangle représentant la taille de la carte. Dans l'onglet *Propriétés de l'objet*, spécifier si besoin sa taille et son orientation, mais surtout **son échelle**.



## Ajouter une échelle graphique

Sélectionner l'outil ![icone échelle](images/mep_05.png)ou *Ajouter un élément* → *Ajouter Echelle graphique* et tracer un rectangle sur la carte. Dans l'onglet *Propriétés de l'objet*, spécifier le nombre et la longueur des segments qui la composent.



## Ajouter une flèche nord

Sélectionner l'outil ![icone nord](images/mep_06.png) ou *Ajouter un élément* → *Ajouter Flèche du nord* et tracer un rectangle sur la carte. Dans l'onglet *Propriétés de l'objet*, aller dans *Rechercher dans les répertoires* pour utiliser l'un des nord par défaut de QGIS. 

Pour utiliser votre propre nord au format svg, toujours dans *Propriétés de l'objet*, aller à *Propriétés principales* > *Source de l'image* et cliquer sur pour accéder à votre fichier. 



## Exporter la carte

Sélectionner l'outil ![icone export pdf](images/mep_07.png) pour exporter en pdf ou l'outil ![icone export svg](images/mep_08.png) pour exporter en svg.



## Pour aller plus loin

Il est fortement recommandé de suivre la formation Niveau 2 Les Figures du rapport avec QGIS. 

