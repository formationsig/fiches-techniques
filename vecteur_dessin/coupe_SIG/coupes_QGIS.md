![logo inrap](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![](images/CC.png)  

[Inrap](https://www.inrap.fr/) - Caroline Font, Thomas Guillemard, Léa Roubaud, Christelle Seng - Novembre 2024

> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).



# Coupes stratigraphiques et QGIS

![][logo_superQGIS] **Niveau hard-core du SIG**  :muscle: :x: :100:

___

## Principe et structuration

La vectorisation et l'enregistrement des relevés de coupe dans une base de données spatiales *via* QGIS a de nombreux avantages. Parmi ceux-ci, les plus évidents sont :
* la possibilité de faire du traitement de données spatiales en exploitant l'enregistrement stratigraphique des archéologues 
* l'exploitation possible des informations spatiales et géométriques (surface, profondeurs et altitudes...) qui sont accessibles par des requêtes dans la base de données
* la numérisation à l’échelle 1:1, permet, grâce à l'outil de mise en page de QGIS, d’exporter à l'échelle de l'illustration souhaitée
* la possibilité de gérer les symboles selon les attributs des données sur l’ensemble des entités numérisés pour une mise en page homogène sur l'ensemble des données
* l'exploitation des outils d'atlas et de variables dans QGIS qui permet de générer des illustrations associant plan(s), coupe(s) et tout autre élément de l'outil de mise en page de QGIS (table attributaire, image, etc.)

### Principe

Les coupes stratigraphiques sont par définition des données d'élévation verticale. La 3<sup>e</sup> dimension est pour le moment assez anecdotique dans QGIS mais il est néanmoins possible de détourner l'usage du logiciel pour permettre la visualisation et l'exploitation de données en élévation. Il s'agit d'utiliser le repère orthonormé du canevas (avec une abscisse en x et une ordonnée en y, selon le système de coordonnées de référence -SCR-) en interprétant les valeurs x et y autrement. 

Dans le SCR Lambert93 (EPSG :2154) les coordonnées qui concernent la France métropolitaine, se trouvent comprises entre 99040 et 1242440 mètres en x et 6046546 et 7110479 mètres en y. Cependant, l'utilisateur n'est pas contraint à cette emprise et peut choisir de dessiner à n'importe quel endroit du canevas. Pour les données d'élévation, nous avons souhaité que les coordonnées x et y conservent un sens que l'on peut relier à une réalité : 
* la coordonnée x correspond à une distance relative le long d'un axe (axe de relevé par exemple);
* la coordonnée y correspond à une altitude absolue selon le nivellement général de la France (NGF-IGN69). 

Les données d'un projet contenant plan et coupe occupera donc sur le canevas deux espaces : 
* le plan, à son emplacement géographique absolu (en France métropolitaine par exemple pour l'EPSG2154)
* l'ensemble des coupes, proches de l'origine du repère orthonormé, qui, pour l'EPSG2154, se trouve aux alentours du golf de Guinée

![Localisation de données en plan et en coupe sur le canevas de QGIS][canevas_plan_coupe]

Si l'opérateur veut consulter à la fois le plan et les coupes, l'utilisation du même SCR est malheureusement obligatoire. Du fait de la déformation induite par le système de projection en plan du sphéroïde du globe terrestre, il n'est pas possible d'utiliser des outils de mesure qui tiennent compte de l'ellipsoïde (l'algorithme de projection de sphéroïde vers un plan). Quelques précautions sont donc nécessaires pour éviter des déconvenues et des erreurs de calculs. 

### Précautions à prendre avec les mesures

Choisir le SCR EPSG2154 n'est pas forcément adapté pour les données d'élévation puisque les coordonnées x et y obtenues localisent nos entités très loin de la France (dans le golfe de Guinée...:fearful:). Le Lambert93 est une projection qui se base sur un cône, ce qui engendre peu de déformation au niveau de la France... Par contre, dans le golfe de Guinée... C'est une autre histoire...

Néanmoins, si l'utilisateur veut afficher dans le même canevas et dans les mêmes mises en page, le plan **ET** la coupe, les données (plan et coupes) **doivent** partager le même système de projection... Néanmoins, si on respecte un certain nombre de gardes-fous, il est possible de contourner les problèmes : 

> :warning: :warning:
> * vérifier que tous les calculs qui se basent sur la géométrie des entités projetés (surface, dimensions, etc.) ne tiennent pas compte de l'ellipsoïde. Cette information est normalement vérifiable dans l'aide des fonctions dans la calculatrice de champs.
> 
> Par exemple, la fonction ```$length``` tient compte de l'ellipsoïde :
> 
> ![calcul longueur avec ellipsoïde](https://gitlab.com/formationsig/sig32/-/raw/master/figures_v2/fonction_length_ellipsoide.png?ref_type=heads)
> 
> Mais la fonction ```length($geometry)``` n'en tient pas compte :
> 
> ![calcul longueur avec ellipsoïde](https://gitlab.com/formationsig/sig32/-/raw/master/figures_v2/fonction_length_ss_ellipsoide.png?ref_type=heads)
> 
> Cette dernière fonction est donc à préférer pour des calculs sur les géométries projetées. 
> :warning: :warning: :warning: **VIGILANCE DONC !!** :warning: :warning: :warning:
> * Le deuxième garde-fou, c'est l'utilisation de l'outil mesure avec le paramètre de **distance cartésienne** sélectionné, en effet, la différence de mesure est vraiment notable : 
> Avec une mesure qui tient compte de l'ellipsoïde : 
>
> ![mesure ellispoide](https://gitlab.com/formationsig/sig32/-/raw/master/figures_v2/mesure_ellipsoide.png?ref_type=heads)
>
> Avec une mesure cartésienne : 
>
> ![mesure ellispoide](https://gitlab.com/formationsig/sig32/-/raw/master/figures_v2/mesure_cartesienne.png?ref_type=heads)
> 
> Ici, pour à peu près la même distance, on a une différence de plus 0,75 m entre les deux !!
> :warning: :warning: :warning: **VIGILANCE DONC !!** :warning: :warning: :warning:
> * troisième garde-fou, dans l'outil de mise en page de QGIS, les échelles des données de coupe doivent **ABSOLUMENT** être paramétrées correctement :
>
> ![paramètre de l'échelle : unités de carte][unite_carte_ech]

### Structuration

Pour des questions de performances et d'optimisation à l'utilisation, une base de données relationnelles et spatiales au format SpatiaLite a été modélisée. Cette base de données inclut les tables d'enregistrement, des requêtes, des fonctionnalités avancées d'automatisation de calculs (*triggers*) et des styles par défaut pour l'affichage dans QGIS. 

![Structure de la base de données spatiale stratigraphique][mld_badass_only_coupe]

> :mag: **NB** : Cette structure constitue une base de travail. Elle peut-être adaptée (ajout de champ) en fonction des problématiques spécifiques d'une opération. PK signifie Primary Key (clé primaire) et FK, Foreign Key (clé étrangère). Par contre, il n'est pas possible de modifier le nom d'un champ, de modifier le typage, de supprimer un champ.

:warning:
Les champ avec le préfixe ```id_``` correspondent aux clés primaires de la table. Elles sont autoincrémentées par le moteur de SpatiaLite. Les champs avec le préfixe ```num``` correspondent aux clés primaires et étrangères établies lors de l'enregistrement par l'archéologue. Les champs ```t_axe."numaxe"```, ```t_us."numus"```, ```t_log."numlog"```, possèdent des contraintes d'unicité et de non nullité. Ce qui signifie qu'**ils doivent obligatoirement être saisis et sans doublon**. 

:warning: :warning:
La clé étrangère ```numaxe``` présente dans les tables d'enregistrement des coupes doit être **OBLIGATOIREMENT** saisie pour **TOUTES** les entités enregistrées dans *coupe_axe*, *coupe_line* et *coupe_poly*.

#### Les axes de relevés en plan (couche *t_axe*)

Cette couche correspond aux levés topographiques des axes en plan. Elle est alimentée par le shape fourni par le topographe à l'issu d'un levé topographique. Un simple copier/coller permet de récupérer les données pour peu que la structure du shape fourni par le topographe corresponde à celle de *t_axe*.
Quelques vérifications des géométries, au préalable, sont nécessaire pour s'assurer que les processus qui suivent se passeront sans encombre : un segment, une orientation, une longueur.

En effet, il s’agit d’une couche de lignes, chacune constituée d’**un seul segment** (c’est-à-dire d’un point d’origine et d’un point de destination, deux points en tout). Par ailleurs, **Un segment possède une direction qui correspond au sens de lecture de la coupe** et une longueur qui doit à peu près correspondre à ce qu'indique la minute de relevé.

> :exclamation: **Attention** : si les axes sont constitués de plusieurs segments, il faut préalablement les exploser en autant de segments que possédés par la ligne. Dans la Boîte de traitement, utiliser l’outil suivant : Géométrie vectorielle/Exploser
des lignes.  
> 
>![Outil Exploser des lignes dans la boîte à outils][002]
>  
>Cette outil génère une couche temporaire qui remplacera la couche d’axe d’origine.  
Il est important de veiller au sens de dessin de la coupe. En Occident, nous lisons les relevés de gauche à droite. Le premier point de l’axe doit donc correspondre au point gauche de lecture de la coupe.
Pour vérifier le sens de la ligne, il suffit d'appliquer un symbole de flèche sur la ligne. 
> 
> ![Paramétrage du symbole de flèche][003]  
> 
> En mode édition, il est possible d'inverser le sens de la ligne en utilisant l'outil d'inversion de ligne présent dans la barre d'outils de numérisation avancée (depuis la version 3.8 de QGIS).  
>
>![Outil d’inversion du sens de la ligne][004]
>

À partir de cette couche vectorielle, plusieurs données attributaires et géométriques permettent de constituer la base de données de numérisation des informations stratigraphiques en coupe :
* la clé primaire archéologique ```t_axe.numaxe``` des axes en plan constitue l'identifiant de relation avec l'ensemble des couches de la base de données sous forme de clé étrangère
* l'altitude de l'axe de relevé disponible dans les attributs des points de levés topographiques (moyenne des deux points d'axe, puisqu'un axe de relevé est forcément horizontal)

#### Axe de relevé en coupe (couche *coupe_axe*)

Il s’agit d’une couche de ligne, également constituée d’**un seul segment** (c’est-à-dire d’un point d’origine et d’un point de destination, deux points en tout).
La dimension de l’axe de relevé en coupe doit correspondre à celle de la minute à l’échelle 1:1.
La relation avec l'axe en plan est établie par la saisie de la clé étrangère correspondant au n° d'axe dans le champ ```coupe_axe."numaxe"```.  
La géométrie de l'axe en coupe sert de référence pour le géoréférencement de la minute sur ses deux extrémités.
Cette dernière est générée avec les propriétés suivantes : 
* le x indiqué sous le canevas correspond à un x relatif à l'ensemble des axes de l'opération espacé de 5 mètres les uns des autres 
* le y correspond à l'altitude absolue exprimée en mètres NGF-IGN69

> :mag: Pour éviter un dessin manuel de l'ensemble des axes, la base de données inclue une vue *vue_coupe_axe*, dont le contenu est le résultat d'une requête SQL sur les tables de la base. Elle est donc automatiquement mise à jour au fur et à mesure des saisies.

```sql
DROP VIEW IF EXISTS vue_coupe_axe; -- le nom de la vue
CREATE VIEW vue_coupe_axe AS -- création de la vue
SELECT 
   "id_axe" -- la clé primaire de SpatiaLite
   ,"numaxe" -- la clé primaire archéologique
   ,"nomaxe" -- le nom de l'axe attribué par les archéologues
   ,degrees(st_azimuth(st_startpoint(geometry),st_endpoint(geometry))) as azimuth -- calcul de l'orientation rapport au nord (azimuth)
   ,st_length(geometry) as long_axe -- calcul de la longueur de l'axe
   ,"alti" -- l'altitude de l'axe
   ,"note" 
   ,makeline(makepoint((SUM(st_length(geometry)) OVER(ORDER BY "id_axe" ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))-st_length(geometry)+"id_axe"* 5 - 5 , "alti"),
makepoint((SUM(st_length(geometry)) OVER(ORDER BY "id_axe" ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+"id_axe"* 5 - 5 ,"alti")) as geometry -- calcul de la géométrie en élévation
FROM t_axe; -- depuis la table *t_axe*
```

:warning: le nettoyage de la table *t_axe* tel que décrit plus haut (vérification du nombre de segment, des longueurs, de l'orientation) doit impérativement avoir été achevé avant d'utiliser le contenu de cette vue !! Une fois les vérifications faites sur *t_axe*, il suffit de copier tout le contenu de cette vue pour le coller dans la couche *coupe_axe*.

Le schéma ci-dessous explique la génération de la ligne de la coupe en élévation. Initialement, l'intervalle choisi était fixé à 2 mètres. Ce sera 5 mètres si vous utilisez la vue *vue_coupe_axe*.

![Numérisation de l’axe en coupe][005]

#### Les entités linéaires en coupe (couche *coupe_line*)

Il s’agit d’une couche vectorielle de polylignes, ou de multipolylignes, et représentent les interfaces observées ou restituées. Elles sont essentiellement de trois natures : 
* la limite d’observation de la coupe (limite de décapage, profondeur du sondage…), souvent représentée en trait-point
* les US négatives (limite de creusement par exemple), souvent représentées par un trait plus épais
* les restitutions d’interfaces d’US, souvent représentées par un pointillé 
La liste de type d'US peut-être complétée en passant par la table *thesaurus* inclue dans la base de données. 

La relation entre les lignes en coupe et l'axe, s'établit selon le champ ```coupe_line."numaxe"```. D'autres clés étrangères sont présentes dans la table et leur saisie est facultative.

![Numérisation des lignes d’interfaces][006]

#### Les surfaces en coupe (couche *coupe_poly*)

Il s’agit d’une couche vectorielle de polygones, ou de multipolygones, qui représente les surfaces observées ou restituées sur la coupe. Ces surfaces sont essentiellement de deux natures : 
* les US 
* les inclusions (mobilier, matériaux divers, écofacts…). 

Le champ *"detail"* permet d'ajouter des détails sur la nature de l'US ou de l'inclusion si aucune base de données d'enregistrement n'existe par ailleurs.  
La clé étrangère ```coupe_poly."numaxe"``` permet de relier ces surfaces à l’axe de relevé.

![Numérisation des surfaces en coupe][007]

## Quelques conseils pour la numérisation

S'agissant d'une base de données spatiales, la numérisation revêt un sens scientifique, à savoir, l'interprétation stratigraphique d'une coupe. Le respect des règles de topologie s'applique donc également pour le dessin en coupe et doit refléter les relations stratigraphiques existantes entre les objets.  Ainsi, et à titre d'exemples, les US en relations stratigraphiques sont dessinées en **adjacences** les unes des autres, les US négatives possèdent un tracé **identique** à l'interface de la ou des US constituant un fait et les inclusions, par définition, incluses dans une US, sont dessinées **sur** les US. 

Plusieurs méthodes de dessin sont possibles pour garantir à la fois un confort de dessin (avec des courbes de Bézier par exemple) et le respect des règles de topologie. Pour faciliter le dessin et en augmenter la vitesse d'exécution nous préconisons la méthode décrite ci-après selon un ordre précis.

### Dessin des axes en coupe

Comme expliqué [plus haut](https://gitlab.com/formationsig/fiches-techniques/-/blob/master/vecteur_dessin/coupe_SIG/coupes_QGIS.md#axe-de-relev%C3%A9-en-coupe-couche-axe_coupe), il est possible de générer automatiquement cette couche à partir de la couche **t_axe** et après avoir fait les vérifcations d'usage.  

Au cas où vous feriez le choix de dessiner les axes en coupe de manière manuelle, la manipulation suivante est possible : 

> :mag: **Utilisation des outils de numérisation avancée de QGIS** :
>  
>![Utilisation de l’outil de numérisation avancée][008]
>
>1. En session d'édition, activer l'outil
>2. Dessiner le premier point sur le canevas en spécifiant les coordonnées x (à 0 pour le premier axe et à distance du précédent axe pour ceux qui suivent) et y = altitude. Ne pas oublier de verrouiller (touche Entrée) les coordonnées avant de sortir de la fenêtre de numérisation avancée.
>3. Saisir la dimension du segment
>4. Verrouiller la dimension (touche Entrée) 
>5. Dessiner le second point du segment à l'horizontal du premier grâce aux guides de dessin de l'outil de numérisation avancée

:exclamation: Pour rappel (insistant), il est absolument impératif de saisir le champ "numaxe" qui permettra de faire la relation avec les axes en plan.

### Géoréferencement de la minute sur la coupe

Idéalement, l'image raster représentant la coupe ne contient qu'une seule coupe pour faciliter l'archivage des données. Si le temps n'est pas suffisant pour individualiser les coupes sur un scan, il faudra créer autant de fichier géoréférencés que de coupes représentées sur ce dernier.

Le géoréférencement s'effectue avec la méthode Helmert sur deux points correspondant aux extrémités de la coupe. Activer l'accrochage sur les sommets d'**axe_coupe** pour plus de précision.  

![Géoréferencement de la minute][009]

Ne pas hésiter à vérifier les dimensions d'un carreau de la minute après le géoréférencement.

> :exclamation: **Attention RAPPELS !** :  
>Si vous voulez prendre des mesures sur la coupe avec l'outil de mesure de QGIS, il faut le paramétrer pour obtenir des mesures Cartésiennes et non Ellipsoïdales. Les mesures ellipsoïdales tiendraient comptes des déformations du SCR qui n'ont pas lieu d'être en coupe.  
>
>![Outil de mesure en mode cartésien][010]

### Numérisation des interfaces et des surfaces

Comme évoqué plus haut, à terme, seules les US négatives, limites d'observation et restitutions sont conservées dans la couche **coupe_line**. Pour faciliter le dessin tout en garantissant un respect des règles de topologie, nous proposons une méthode de dessin qui permet à la fois d'utiliser les courbes de Bézier et de garantir un dessin sans erreur de géométrie ou de topologie. Dans la couche **coupe_poly**, figurent les US et les inclusions.  
Cette méthode nécessite d'utiliser les couches temporaires.

> :mag: **Rappel sur les couches temporaires** :
>  
>![Créer une couche temporaire][011] 
>
> Les couches temporaires dans QGIS permettent de créer des fichiers de formes (points, lignes, polygones, ou même tables sans géométrie) qui, comme leur nom l'indique, ne sont pas enregistrées. Ces couches sont idéales pour dessiner des objets qui à terme ne seront pas conservés : des lignes de construction par exemples ou des résultats intermédiaires de calculs.  
> :exclamation: **Attention** : si ces couches ne sont pas enregistrées leur contenu disparaît à la fermeture du projet (y compris lors d'un plantage du logiciel).

1. Créer une couche temporaire de lignes ![Créer une couche temporaire][011]. Le nom n'est pas important puisque celle-ci ne sera pas conservée.
2. Dessiner sur cette couche la limite de décapage ou la limite supérieure de la coupe. Il est possible d'utiliser les courbes de Bézier pour respecter la courbe du dessin d'origine.  
![Dessin de la limite de décapage][012]
3. Activer l'accrochage sur les segments de la couche temporaire  
![Activation de l’accrochage sur la ligne temporaire][013]
4. Dessiner les limites verticales et horizontales de lecture de la coupe. L'utilisation de l'outil de numérisation avancée peut permettre de dessiner en se servant des guides verticaux et horizontaux (surlignés en jaune ci-dessous).  
![Utilisation des guides de l’outil de numérisation avancée][014]
5. Dessiner **toutes** les interfaces stratigraphiques en veillant à accrocher sur les segments pour respecter la topologie.
![Dessin des interfaces stratigraphiques de la coupe][015]
6. Sélectionner parmi les interfaces dessinées les seules : limites d'observation, US négatives et restitutions et les copier
7. Coller ces lignes sur la couche **coupe_line**
8. Avant de déselectionner les objets collés, il est possible de compléter par lot la table attributaire ![Outil de saisie d’attributs par lot][016]. Compléter la clé étrangère de numéro d'axe sur l'ensemble des lignes copiées est ainsi également plus simple.
9. Remplir les autres données attributaires nécessaires (*"typline"*, *"numfait"* et *"numus"* pour les US négatives)
10. Sur la couche de ligne temporaire, lancer le model3 (*coupe_line_vers_poly*) fourni en pièce jointe

> :mag: **NB** : Ce model3 permet de lancer une succession de traitements sur la couche de lignes temporaires :  
>Les lignes sont dans un premier temps légèrement prolongées aux extrémités pour créer une intersection (outil *Géométrie vectorielle/Prolonger les lignes*) puis des polygones sont créés pour chaque espace ainsi délimité (outil *Géométrie vectorielle/Mise en polygone*)  
>
>![Model3 de conversion des lignes vers des polygones][017]
> Le modèle inclu la possibilité de modifier la dimension de prolongation des lignes au cas où la dimension par défaut ne produirait pas le résultat escompté.

11. Une nouvelle couche temporaire est créée. Sélectionner tous les polygones qui concernent la coupe dessinée, les copier et les coller dans la couche **coupe_poly**.  
![Résultat du model3][018]
13. Remplir les données attributaires, par lot ![Outil de saisie d’attributs par lot][016] et individuellement avec l'outil Identifier des entités ![Outil identifier des entités][019] 
14. Dessiner les éventuelles inclusions sur la couche **poly_coupe** avec l'outil souhaité (ajout de polygone ou courbe de bézier)

Votre coupe est terminée !

![Exemple de coupe achevée][020]

## Mise en page

Il est possible d'utiliser les mises en pages pour préparer des planches d'illustrations complexes qui incluent à la fois une vignette de localisation du fait, le plan, la coupe, des images et des extraits de table attributaire. Nous vous présenterons ci-après deux méthodes pour faciliter l'incorporation des coupes :  
* en générant un atlas
* en utilisant des variables

### Générer un atlas

Pour rappel, l'atlas permet de répéter une mise en page en déplaçant automatiquement le contenu des cartes selon une couche de couverture. En toute logique, la couche de couverture à privilégier pour un atlas incorporant les coupes serait la couche des axes en plan (**axe_plan**). La planche d'illustration comporterait *a minima* un premier élément carte pour la représentation en plan du ou des faits archéologiques et des axes de relevés et un autre élément carte pour la coupe. Le contenu de ces deux cartes se répondent selon la coupe concernée. Cependant, l'emprise en plan ne se trouve pas au même endroit en coupe. Il y aurait donc en théorie deux couches de couverture différentes : une pour le plan, une pour la couche. C'est impossible actuellement de paramétrer deux couches de couverture dans QGIS. Par contre, il est possible de contrôler l'emprise du contenu d'une carte dans les Propriétés de l'élément.

![Propriété d’emprise de contenu d’un élément carte][021]

A partir de cette fonctionnalité, l'idée est de calculer l'emprise de la coupe liée à l'axe en plan et de faire varier cette emprise pour chacune des pages de l'atlas. Plusieurs solutions sont possibles pour aboutir à ce résultat, toutes impliquent l'utilisation avancées des calculs en langage SQL.

Partant de ce principe, les étapes proposées de mise en place de l'atlas sont les suivantes :
1. Création de la couche de couverture en virtual layer ![Création d’une couche virtuelle][022] qui comporte les données géométriques de la couche **axe_plan** et, par jointure, les coordonnées x_min, x_max, y_min et y_max de la couche **axe_coupe**. La formule SQL est la suivante :  
```sql
select p."id_axe", X(start_point(c.geometry)) as min_X, Y(start_point(c.geometry)) as min_Y, X(end_point(c.geometry)) as max_X, Y(end_point(c.geometry)) as max_Y, p.geometry as geometry  
from axe_plan as p  
join axe_coupe as c  
ON p."id_axe" =  c."id_coupe"
```
2. Dans la mise en page, paramétrer l'atlas (1) avec, en couche de couverture, la couche virtuelle créée précédemment (2). Préciser le nom de la page comme étant l'identifiant (3) d'axe et ordonner par ce numéro (4).  
![Paramétrage de l’atlas][023]
3. Ajouter les différents éléments à votre mise en page, *a minima* :
* une carte dont le contenu est contrôlé par l'atlas qui contient le plan des faits archéologiques et les coupes associées  
![Carte contrôlée par l’atlas][024]
* une carte dont l'emprise est fonction des champs calculés dans la couche virtuelle (min_X, min_Y, max_X et max_Y) qui contient la coupe correspondante  
![Carte dont l’emprise est fonction des valeurs des données attributaires de la couche de couverture][025]
4. Générer l'atlas

> :mag: **NB** : nous vous conseillons d'utiliser les variables liées à l'atlas dans la configuration des symboles des couches présentes dans la mise en page. Elles vont permettront par exemple de ne faire apparaître que l'axe concerné par la coupe (`"id_axe" = @atlas_pagename`) d'afficher ou de mettre en surbrillance le fait concerné par des requêtes d'intersection : `intersects( $geometry,  @atlas_geometry )`.

En passant un peu de temps sur la mise en page de la première planche, on peut aboutir un niveau de complexité et de finition directement publiable.

![Exemple de planche d’atlas achevée][026]

### Utilisation de requêtes SQL et de variables

Dans le cas plus spécifique où plusieurs coupes sont attendues sur une même planche, pour illustrer un bâtiment sur poteaux par exemple, il est également possible "d'appeler" des coupes dans une mise en page. Comme pour le chapitre précédent, nous utiliserons la même couche virtuelle et les paramètres d'emprise définis par les données.

1. Si ce n'est pas fait, créer la couche virtuelle selon la requête suivante :  
```sql
SELECT p."id_axe", X(start_point(c.geometry)) AS min_X, Y(start_point(c.geometry)) AS min_Y, X(end_point(c.geometry)) AS max_X, Y(end_point(c.geometry)) AS max_Y, p.geometry AS geometry  
FROM axe_plan AS p  
JOIN axe_coupe AS c  
	ON p."id_axe" =  c."id_coupe"
```
2. Dans la mise en page, ajouter un élément carte et le renommer selon l'*"id_axe"* de la coupe à appeler. 
> :mag: **NB** : malheureusement il n'est pas toujours possible de numéroter un axe selon le fait concerné, plusieurs faits pouvant être traversés par un même axe et plusieurs axes pouvant traverser un même fait (relation de n à n). C'est pour cette raison que la clé primaire ne peut-être le numéro de fait. Afin de trouver facilement quels sont les numéros d'axe associés à quel(s) fait(s) archéologique(s), nous vous conseillons d'utiliser un tableau de jonction. Celui-ci est très facile à générer en utilisant la jointure spatiale ![Jointure spatiale][027].  
>![Paramétrer la jointure spatiale pour obtenir un tableau de correspondance « id_axe » ↔ « numpoly »][028]  
> le tableau obtenu ressemble à ça :  

| *"id_axe"* | *"numpoly"* |
| -------------------------- | ------------------------ |
| 2 | 150 |
| 3 | 160 |
| 4 | 3 |
| 5 | 4 |
| 6 | 5 |
| 7 | 6 |
| 8 | 61 |
| 9 | 80 |
| 10 | 7 |
| 11 | 79 |
| 12 | 25 |
| 13 | 78 |
| 14 | 26 |
| 14 | 85 |
| 15 | 77 |
| 16 | 77 |

> En filtrant sur le numéro de fait, on obtient facilement les numéros d'axes concernées.

3. Dans les propriétés de l'élément carte, et les paramètres d'emprise, sélectionner le menu Éditer... de la boîte de données définies par les valeurs de chaque coordonnée  
![Editer les coordonnées d’emprise de la carte][029]
4. Remplir comme suit les quatre paramètres de coordonnées :
* coordonnée min_X, la requête sera la suivante :  
```sql
attribute(get_feature('nom_de_la_couche_virtuelle','id_axe',@map_id),'min_X')
```
* coordonnée min_Y, la requête sera la suivante :  
```sql
attribute(get_feature('nom_de_la_couche_virtuelle','id_axe',@map_id),'min_Y')
```
* coordonnée max_X, la requête sera la suivante :  
```sql
attribute(get_feature('nom_de_la_couche_virtuelle','id_axe',@map_id),'max_X')
```
* coordonnée max_Y, la requête sera la suivante :  
```sql
attribute(get_feature('nom_de_la_couche_virtuelle','id_axe',@map_id),'max_Y')
```

> :mag: **NB** : plusieurs éléments de cette requête sont importants à commenter :  
>* `attribute('entité', 'champ')` : cet opérateur permet de récupérer l'attribut du champ qui contient la coordonnée souhaitée pour l'entité précisée en premier argument 
>* `get_feature('couche','champ','valeur')` : permet d'inclure une couche et une entité du projet dans la requête en précisant les arguments suivants :  
>   * 'couche', *couche* étant à remplacer par le nom de la couche virtuelle créée en 1
>   * 'champ' permet de préciser quel est le champ qui permet de trouver l'objet souhaité, ici *"id_axe"* 
>   * 'valeur', doit contenir le numéro de l'axe souhaité, on pourrait rentrer directement le numéro de l'axe mais on peut également utiliser les variables pour ne pas avoir à retoucher à chaque élément carte la requête. Nous proposons d'utiliser la variable `@map_id` qui correspond au nom courant de l'élément carte. D'où l'importance de renomment l'élément carte comme décrit dans le point 2.

5. Normalement, le contenu de la carte doit se mettre à jour en recentrant l'emprise sur la coupe du fait souhaitée. Si rien ne se passe, essayer d'actualiser la mise en page.
6. Si vous voulez insérer une nouvelle coupe dans cette mise en page, il suffit de copier/coller l'élément carte paramétrée dans les points précédents et d'en changer le nom pour qu'il corresponde au numéro de l'axe souhaité. Une nouvelle mise à jour de la mise en page devrait en changer le contenu.

Il est ainsi très facile de composer des mises en page complexes :

![Exemple de mise en page avec plusieurs coupes][030]

## Conclusion

Le traitement des coupes dans une base de données spatiales constitue une vraie opportunité d'aborder tous les aspects de l'enregistrement archéologique dans l'argumentaire scientifique. Il est désormais possible de constituer une base de données spatiales qui regroupe à la fois le plan issu du levé topographique et de relevés de terrain, les coupes issues des relevés et l'enregistrement attributaire descriptif.  
Hormis l'avantage certain de gérer l'ensemble de l'export d'illustrations complètes et complexes sans jongler entre les logiciels, cette dimension supplémentaire permet de formuler des requêtes et de les visualiser en direct sur l'ensemble des données stratigraphiques d'une opération archéologique.  
Parmi les requêtes attributaires possibles, il devient facile de visualiser les données stratigraphiques selon la nature des US, la présence de mobilier par cercles proportionnels, les textures... autant de descriptifs nécessaires à l'interprétation archéologique d'une occupation humaine. La dimension spatiale permet d'enrichir les données archéologiques par des requêtes géométriques : calcul des profondeurs automatiques, calcul de pente sur plusieurs coupes pour un fossé, support d'interpolation de surfaces... L'expérience et l'expérimentation compléteront par d'autres exploitations cette première liste.  
Le dessin des coupes n'est plus une simple mise au propre des minutes de terrain, il devient un traitement scientifique à part entière ce qui apporte une véritable valeur ajoutée au travail des archéologues sur le terrain et en phase d'étude. 

   
## Dessin automatique des logs stratigraphiques

La base de données inclue des tables et vues qui permettent de générer automatiquement les logs stratigraphiques. Ces tables figurent sur le modèle logiques plus haut. Il s'agit des tables :
* *t_us* : permet de décrire de manière succinte les US qui constituent le log stratigraphique. Cette table peut également accueillir les données d'enregistrement stratigraphique de l'opération
* *t_log* : permet d'enregistrer les logs stratigraphiques et leur géométrie, fournie par le topographe
* *j_us_log* : permet d'enregistrer l'association d'une US avec un log et d'en saisir les propriétés géométriques au sein de ce log spécifique.

L'enregistrement se déroule de la manière suivante :

1. les US sont enregistrées dans la table *t_us*
2. les logs et leur géométries sont enregistrés dans la table *t_log*. *A minima* et outre le numéro qui, te toute façon est obligatoire, il faudra saisir l'altitude du sommet du log dans le champ ```t_log."alti"``` :one: et la profondeur du log dans le champ ```t_log."prof_log"``` :two:. Une automatisation de calcul implémentée dans la base de données ajoutera la valeur d'altitude correspondante dans le champ ```t_log."zmin_log"```. Pas besoin donc de saisir quoique ce soit dans ce champ, c'est la base de données qui s'en charge. :warning: :warning: **Surtout, NE PAS OUBLIER D'ENREGISTRER LE LOG AVANT DE PASSER À LA SUITE !** :three:
3. il est ensuite possible d'associer les US au log **DANS L'ORDRE D'OBSERVATION DU HAUT VERS LE BAS** dans le sous-formulaire de la table *t_log* :four:. 

![sous-formulaire d'enregistrement des US dans le log][t_log_ssform]

### Association des US aux logs

Une série de *triggers* ont été incorporées à la base de données pour automatiser certains calculs dans la base de données. Ces déclencheurs permettent un minimum de saisie, le reste étant complété automatiquement par la base de données.

1. La première US observée (la plus haute dans le log) doit être ajoutée en premier en cliquant sur le bouton **Ajouter une entité fille** du sous-formulaire

![ajout de la première associée au log][t_log_ssform_b]

2. Saisir ensuite le n° de l'US :one:
3. Saisir la profondeur du toit de l'US (qui sera toujours de 0, logiquement) :two:
4. Saisir la profondeur de la base de l'US (:warning: le séparateur de décimal est la virgule) :three:
5. Enregistrer avant d'ajouter une nouvelle US dans le log :four:

![ajout de la première associée au log][t_log_ssform_c]

6. Pour les US qui suivent, seule le n° d'US :one: et la profondeur de la base de l'US :two: sont à saisir
7. NE PAS OUBLIER D'ENREGISTRER ENTRE CHAQUE AJOUT D'US !! :three:

Le contenu des autres champs sera complété par la base de données.

![La table j_us_log complétée][j_us_os_full]

C'est à partir de cette table que la géométrie des logs est reconstituée dans la vue vue_log_stratigraphique en s'appuyant sur une requête SQL :

```sql
DROP VIEW IF EXISTS vue_log_stratigraphique;
CREATE VIEW vue_log_stratigraphique AS
SELECT 
    t.*,
    l.numtr,
    l.zmin_log, 
        CASE 
        WHEN "zmin_uslog" is null and t."zmax_uslog" <> l."zmin_log" THEN BuildMbr(t."numlog"-1, l."zmin_log", t."numlog"-0.75, "zmax_uslog", 2154) 
        WHEN "zmin_uslog" is null and t."zmax_uslog" = l."zmin_log" THEN BuildMbr(t."numlog"-1, t."zmax_uslog"-0.10, t."numlog"-0.75, t."zmax_uslog", 2154)
        ELSE BuildMbr(t."numlog"-1, "zmin_uslog", t."numlog"-0.75, "zmax_uslog", 2154) 
        END AS geometry 
FROM j_us_log AS t 
JOIN t_log AS l 
ON t."numlog" = l."numlog";
```

Et le résultat visuel est le suivant :
![Géométrie générée par la vue vue_log_stratigraphique][dessin_log]

Les logs se placeront les uns à côté des autres en fonction de leur altitude respective.

## Les autres vues...

D'autres vus sont présentes dans la base de données et permettent d'exploiter au maximum l'intégration de la géométrie des coupes stratigraphiques.

### Visualiser les coupes sur le plan à côté de l'axe

![coupes sur le plan à côté de l'axe][031]

```sql
--Vue de translation des coupes sur le plan
--pour les axes
DROP VIEW IF EXISTS vue_axe_coupe_translate;
CREATE VIEW vue_axe_coupe_translate AS 
SELECT a."id_axe",a."numaxe",c."numinute",c."azimuth",c."alti", 
st_translate(c."geometry", x(startpoint(a."geometry"))-x(startpoint(c."geometry")),y(startpoint(a."geometry"))-y(startpoint(c."geometry")),0) AS geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM coupe_axe AS c 
JOIN t_axe AS a ON a."numaxe" = c."numaxe";

--pour les lignes
DROP VIEW IF EXISTS vue_line_coupe_translate;
CREATE VIEW vue_line_coupe_translate AS 
SELECT l."id_cpline",a."id_axe",a."numaxe","numfait","numus","typline", 
st_translate(l."geometry", x(startpoint(a."geometry"))-x(startpoint(c."geometry")),y(startpoint(a."geometry"))-y(startpoint(c."geometry")),0) AS geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM coupe_line AS l 
JOIN coupe_axe AS c ON c."numaxe" = l."numaxe" 
JOIN t_axe AS a ON a."numaxe" = l."numaxe" ;

--pour les polygones
DROP VIEW IF EXISTS vue_poly_coupe_translate;
CREATE VIEW vue_poly_coupe_translate AS 
SELECT p."id_cpoly",a."id_axe",a."numaxe","numfait","numus","typoly","detail", 
st_translate(p."geometry", x(startpoint(a."geometry"))-x(startpoint(c."geometry")),y(startpoint(a."geometry"))-y(startpoint(c."geometry")),0) as geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM coupe_poly AS p 
JOIN coupe_axe AS c ON c."numaxe" = p."numaxe" 
JOIN t_axe AS a ON a."numaxe" = c."numaxe";
```

### Visualiser les coupes sur le plan en tenant compte de l'orientation de l'axe

![coupes sur le plan en tenant compte de l'orientation de l'axe][032]

```sql
--Vue de translation et rotation des coupes sur le plan
--pour les lignes
DROP VIEW IF EXISTS vue_line_coupe_translate_rotate;
CREATE VIEW vue_line_coupe_translate_rotate AS 
SELECT l."id_cpline",a."id_axe",a."numaxe","numfait","numus","typline","numsd", 
shiftcoords( rotatecoords( shiftcoords(l.geometry,-x(startpoint(a.geometry)),-y(startpoint(a.geometry))) ,c.azimuth-90) ,x(startpoint(a.geometry)),y(startpoint(a.geometry))) AS geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM vue_line_coupe_translate AS l 
JOIN coupe_axe AS c ON c."numaxe" = l."numaxe" 
JOIN t_axe AS a ON a."numaxe" = l."numaxe" ;

--pour les polygones
DROP VIEW IF EXISTS vue_poly_coupe_translate_rotate;
CREATE VIEW vue_poly_coupe_translate_rotate AS 
SELECT p."id_cpoly",a."id_axe",a."numaxe","numfait","numus","typoly","detail","numsd", 
shiftcoords(rotatecoords( shiftcoords(p.geometry,-x(startpoint(a.geometry)),-y(startpoint(a.geometry))) ,c.azimuth-90) ,x(startpoint(a.geometry)),y(startpoint(a.geometry))) as geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM vue_poly_coupe_translate AS p 
JOIN coupe_axe AS c ON c."numaxe" = p."numaxe" 
JOIN t_axe AS a ON a."numaxe" = c."numaxe";
```

### visualisation des US de creusement superposées selon un axe médian

![US de creusement superposées selon un axe médian][033]

```sql
--Vue de translation des US négatives en coupe sur un axe médian  
DROP VIEW IF EXISTS vue_cumulcoupe_us_negative;
CREATE VIEW vue_cumulcoupe_us_negative AS 
SELECT l."id_cpline",t."id_axe", a."numaxe", l."numus", l."numfait", l."numsd", 
shiftcoords(l."geometry", -x(st_centroid(l."geometry"))-10 ,0) AS geometry --requête de translation vers un axe médian à -10 m en X et en y = z
FROM coupe_line AS l 
JOIN coupe_axe AS a ON a."numaxe" = l."numaxe"
JOIN t_axe AS t ON t."numaxe" = l."numaxe" 
WHERE "typline" LIKE 'US négative';
```

## Pièces jointes
* **Model 3** à copier/coller dans le dossier `C:\Users\ "*nom_session*" \AppData\Roaming\QGIS\QGIS3\profiles\default\processing\models` :  
  
   > :warning: Si vous utilisez le navigateur *Google Chrome* tous les fichiers seront téléchargés avec l'extension *.txt* → veillez à remettre l'extension d'origine (*.model3* ou  *.qml*)

   * [bd_coupe.sqlite](https://gitlab.com/formationsig/fiches-techniques/-/blob/master/vecteur_dessin/coupe_SIG/outils/db_coupe.sqlite)
   * [projet_coupe.qgz](https://gitlab.com/formationsig/fiches-techniques/-/blob/master/vecteur_dessin/coupe_SIG/outils/projet_coupe.qgz)
   * [coupe_line_vers_poly.model3](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/vecteur_dessin/coupe_SIG/outils/models/coupe_line_vers_poly.model3?inline=false) → à exécuter sur la couche temporaire des lignes évoquée [ici](#numérisation-des-interfaces-et-des-surfaces). Transforme la couche temporaire de lignes en polygones

[001]:images/image_001.png
[002]:images/image_002.png
[003]:images/image_003.png
[004]:images/image_004.png
[005]:images/image_005.png
[006]:images/image_006.png
[007]:images/image_007.png
[008]:images/image_008.png
[009]:images/image_009.png
[010]:images/image_010.png
[011]:images/image_011.png
[012]:images/image_012.png
[013]:images/image_013.png
[014]:images/image_014.png
[015]:images/image_015.png
[016]:images/image_016.png
[017]:images/image_017.png
[018]:images/image_018.png
[019]:images/image_019.png
[020]:images/image_020.png
[021]:images/image_021.png
[022]:images/image_022.png
[023]:images/image_023.png
[024]:images/image_024.png
[025]:images/image_025.png
[026]:images/image_026.png
[027]:images/image_027.png
[028]:images/image_028.png
[029]:images/image_029.png
[030]:images/image_030.png
[logo_superQGIS]:images/logo_superQ.png
[canevas_plan_coupe]:images/canevas_plan_coupe.png
[unite_carte_ech]:images/unite_carte_ech.png
[mld_badass_only_coupe]:images/mld_badass_only_coupe.png
[t_log_ssform]:images/t_log_ssform.png
[t_log_ssform_b]:images/t_log_ssform_b.png
[t_log_ssform_c]:images/t_log_ssform_c.png
[t_log_ssform_d]:images/t_log_ssform_d.png
[j_us_os_full]:images/j_us_os_full.png
[dessin_log]:images/dessin_log.png
[031]:images/coupe_translate.png
[032]:images/coupe_translate_rotate.png
[033]:images/coupe_cumul_USneg.png

