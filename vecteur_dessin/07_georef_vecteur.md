![](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) <img src="images/CC.png">  

[Inrap](https://www.inrap.fr/), réseau des référents SIG - Sophie Oudry, Frédéric Audouit, Caroline Font, Bertrand Houdusse, Ellebore Segain, Véronique Vaillé (2023)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.28 de QGIS
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).





# Géoréférencer des couches vecteur

Depuis la version 3.26, QGIS permet de géoréférencer des vecteurs sur le même principe que les rasters. Ceci est fort utile et plus simple que l'utilisation de Vector Bender pour importer des plans réalisés sous Illustrator. Plusieurs étapes seront toutefois nécessaires pour tirer profit de cet outil et limiter la quantité d'informations à retravailler. 



# 0. Remarques préalables

- Un certain nombre de problèmes de géoréférencement peuvent venir du traitement sous AutoCAD, n'hésitez pas à revenir vers votre topographe si besoin.
- Le format à utiliser dans Illustrator est le .dxf. Pour l'instant, le format pdf ne gère pas bien l'export des textes.
- Le géoréférenceur n'affichera qu'un seul type de géométrie au choix à l'appel du fichier : les points
  ensemble, les lignes ensemble et les polygones ensemble. Un polygone ayant un fond et un contour définis dans Illustrator apparaître en général deux fois (une dans les polygones pour le fond et une dans les lignes pour le contour)
- Si toutes les géométries semblables sont dans la même couche, après géoréférencement, la couche contient un champ "layer", celui du calque de départ, ce qui permet de faire des tris.
- Attention, ce n'est pas **parfait**, il y a des ratés et il faut donc être vigilants sur les éléments importés ; il est possible que ça évolue avec le temps, n'hésitez donc pas à revenir ici pour vérifier les mises à jour. 



#  1. Travail dans Illustrator

## 1.1. Enlever le superflu

- Dégrouper les objets
- Supprimer les masques d'écrêtage 

En fonction de la finalité de votre géoréférencement, vous aurez à supprimer un certain nombre de choses : 

- pour une intégration dans Caviar, on peut supprimer les points topo et ne garder que ce qui correspond aux éléments suivants : carroyage, emprise, tranchées ou décapage, vestiges, numéros d'ouvertures et de vestiges, logs.



## 1.2. Fonds, contours, etc

- Fermer les formes qui vont devenir un polygone avec Joindre (Ctrl + J), ou Pathfinder → Fusion ou bien importer en lignes et transformer en polygone plus tard  (voir plus bas le 3.3. Facultatif)

- Pour les polygones, appliquer un fond de couleur et pas de contour (ils ne seront importés qu'en tant que polygone)  

- Pour l'emprise/parcelle : un fond et un contour (peut servir en cas de calage uniquement sur du parcellaire)  

- En cas de présence d'un carroyage comportant les coordonnées : s'assurer d'avoir des croix sous forme de lignes ; si c'est du calage de DAO de détails, il vaut mieux avoir l'axe de dessin comme référentiel. Mettre les lignes de carroyage en 0.15 pt.

  

## 1.3. Trier les données 

- Pour les polygones, un calque par type (tranchée, fait, US, emprise)

- Trier les numéros par calques (faits, tranchées) 

- Mettre les numéros **à l'intérieur** des entités



## 1.4. Exporter en dxf

Enfin, exporter le tout en dxf 2000 : cocher "caractère modifiable maximum" pour conserver le texte.

![export dxf](images/07_00.png) 



# 2. Géoréférencement

## 2.1. Préparation et premier géoréférencement

- Préparer le canevas de la carte comme pour un géoréférencement de vecteurs. 

- L'outil est désormais dans le menu Couche → Géoréférencer. Il y a maintenant deux types de fichiers possibles : <img src="images/07_01.png" alt="outil géoréférencer" style="zoom:67%;" />

- On prend ici le 2e et dans la fenêtre qui s'ouvre, on a 3 possibilités quand on va chercher le dxf (polygones, lignes et points)

![ouverture du dxf](images/07_02.png)

Le plan utilisé ici ayant des croix de carroyage avec des coordonnées, il faut commencer par la couche de lignes. L'affichage de cette partie du dxf permet de vérifier rapidement ce qui s'affiche. Dans l'exemple suivant, le cadre contenant les amorces n'a pas été supprimé, il y a également des résidus de vestiges et de tranchées. 



![aperçu géoref du dxf](images/07_03.png)

- Commencer le géoréférencement comme pour un raster. 
- Choisir les paramètres de transformation :

Le fichier par défaut est en format Geopackage (extension en .gpkg), il est préférable de le laisser tel quel et d'aller par la suite puiser les infos dont on a besoin à l'intérieur. 

Comme il y aura plusieurs géoréférencements à la suite, pensez à renommer vos fichiers en fonction du type de géométries appelées.

![paramètres de transformation](images/07_04.png)

- Lancer le géoréférencement ; **NE PAS FERMER LA FENETRE**

On obtient tout ce qui n'avait pas de fond dans Illustrator dans une couche gpkg de lignes :

![import des couches de lignes](images\07_05.png)

*Le décalage visible ici est dû à un support de géoréférencement différent entre l'emprise et les tranchées*



## 2.2. Géoréférencements suivants

Il faut recommencer l'opération pour les polygones, puis pour les points :

- Retourner sur la fenêtre du géoréférenceur, enregistrer les points de contrôle, puis rouvrir le vecteur
- Relancer pour les poly, attention à changer le nom du fichier en sortie 

![import des poly](images/07_06.png)

- Réaliser une dernière fois le géoréférencement pour les points



# 3. Post-traitement 

Si dans la couche de lignes, il n'y avait que le carroyage, vous n'aurez pas à l'utiliser, elle n'a servi qu'au géoréférencement. 



## 3.1. Traiter les polygones

La couche de **polygones** contient, dans le champ *Layer*, le nom du calque dans lequel ces polygones étaient classés dans Illustrator. Il va donc falloir trier les données : les tranchées, les vestiges, etc. 

Pour les récupérer, faire une sélection des tranchées ou des vestiges → Exporter les entités sélectionnées sous → Créer une couche vecteur par type de polygones : ouverture, unobs, etc.

Le processus est le même pour la couche de lignes si vous avez des données à l'intérieur (par exemple les axes). 



## 3.2. Traiter les points

La couche de points est celle qui contient les numéros des entités disponibles dans les couches de ligne ou de polygone. Ces numéros figurent dans le champ *Text* à la fin de la table attributaire. Encore une fois, le champ *Layer* nous indique à quel calque initial ils appartiennent. 

![table attributaire à trier](images/07_07.png)

Quand on étiquette avec la couche de points : 

![étiquetage de la couche de points](images/07_08.png)



Pour récupérer les numéros de tranchées/vestiges/etc, il faut réaliser une **[jointure spatiale](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/03_jointure_spatiale.html)** sur une partie des points. 

***Attention, l'import de polygones depuis Illustrator ne vous met pas à l'abri des erreurs de géométrie, veillez à faire des [vérifications de validité et de géométrie](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/04_verif_geom.html) avant de procéder à la jointure spatiale.***

- Sélectionner les points correspondants dans le champ *Layer* de la couche de points
- Faire une jointure spatiale : Vecteur → Outils de gestion de données → Joindre les attributs par localisation

![jointure spatiale](images/07_09.png)

***Attention à bien ne cocher que les entités sélectionnées dans la couche à joindre :***

![paramètres de la jointure spatiale](images/07_10.png)

Comme indiqué en préambule, il peut y avoir quelques ratés : ici le point de la tranchée 12 n'avait pas bien été positionné dans la tranchée. Il arrive aussi que certains numéros ne soient pas bien importés. 

![point perdu](images/07_11.png)

- Une fois la jointure spatiale faite, il faut compléter la table attributaire notamment pour récupérer les numéros dans le bon champ. 

- Pour récupérer ces numéros sans avoir à faire trop de saisie, on peut s'aider de [formules dans la table attributaire](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/01_calcu_champ.html) :

  ```
  RIGHT("Text", 1)
  ```

  Cette fonction par exemple va renvoyer le dernier caractère à droite du champ *Text* dans le cas des entités à un seul chiffre. Remplacer le "1" par "2" pour les entités à 2 chiffres et ainsi de suite. 



![récupérer les numéros de points](images/07_12.png)



## 3.3. Facultatif : transformer des lignes en polygones 

Il peut arriver que certains polygones ne soient pas bien importés et que leur emprise ne soit présente que sous forme de lignes. Dans ce cas, vous aurez à transformer les lignes en polygones. 

**Attention, il faut commencer par vérifier les géométries**

Vecteur → Vérifier les géométries **ET** Vecteur → Outils de géométrie → Vérifier la validité



Traitement → Boite à outils → Mise en polygones 

Ou bien plus simplement copier les lignes dans une couche de polygones.

**Attention**, évidemment l'outil créera un polygone pour l'emprise, pour le carroyage, etc selon ce qu'il y avait dans votre couche de ligne. Il y a donc du nettoyage à faire !

![polygoniser](images/07_13.png)
