![logo inrap](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![logoCC](images/CC.png)  

[Inrap](https://www.inrap.fr/) - équipe Formateurs & Référents SIG (2021)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS.
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).



# La jointure attributaire

Opération consistant à joindre deux tables grâce à un identifiant commun - relation 1 à 1

> On peut joindre un tableau à une couche vecteur ou inversement, ou encore les tables de deux couches vecteurs. 



- Nettoyer le tableau à joindre avec un tableur (LibreOffice Calc, MS Excel): donner un nom à la feuille, vérifier la longueur des champs, supprimer les caractères spéciaux et les espaces, supprimer les cellules fusionnées...) et **identifier la colonne servant d'identifiant unique**.

- Dans QGIS, ajouter la feuille du tableur (un cliquer-glisser dans la carte suffit).

- Ouvrir la table attributaire de ce tableau pour vérifier l’encodage, la distribution dans les colonnes

- Depuis le Panneau Couches, Faire un Clic droit sur la couche vecteur à laquelle on veut joindre des données → *Propriétés* → ![icon_jointure](images/icon_jointure.png) *Jointures* → ![+](images/iconplus.png) 
  - :one: **Joindre la couche** : choisir le tableau à joindre
  
  - :two: **Champ de jointure** & **Champ dans la couche cible**: choisir l'<u>identifiant unique **identique** dans chaque couche</u>
  
  - :three: Choisir éventuellement les champs à joindre
  
  - :four: **modifier le préfixe**: choisir un préfixe court et explicite ( *J_* ou _ par exemple) ou même éventuellement ne ne pas en mettre)
  
  - Valider avec [OK]
  
    ![jointure attributaire](images/joint_att_01.png) 

> :warning: si une table est jointe à un vecteur, on peut l’interroger, l’afficher, mais on ne
>peut pas faire de calcul sur celle-ci. Il faudra alors créer un nouveau vecteur : 
>
>Clic droit sur la couche → *Exporter* → *Sauvegarder les entités sous*