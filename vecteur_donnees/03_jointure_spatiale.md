![logo inrap](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![](images/CC.png)  

[Inrap](https://www.inrap.fr/) - équipe Formateurs & Référents SIG (2021)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS.
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).



# La jointure spatiale

Permet de joindre les données d’un vecteur à un autre lorsque ceux-ci se superposent (couche axes et couche faits par exemple)



On peut utiliser deux outils selon l'information que l'on souhaite récupérer : 

- soit les données simples
- soit les données avec des calculs ou des statistiques possibles (moyenne, médiane, etc.)



## Cas 1 : récupérer les données simples

>  *Exemple* : on voudrait attribuer aux axes le numéro du vestige qu'ils coupent. 

*Vecteur → Outils de gestion de données →  ![icon_roue](images/icon_roue.png) Joindre les attributs par localisation*

:information_desk_person: tous les outils de traitement disponibles par les Menus le sont aussi *via* la Boîte à outils de traitement: *Traitement* → *Boite à outils*, chercher l'outil adéquat à l'aide de :mag_right: ​la barre de recherche intégrée.

![joindre les attributs par localisation](images\joint_spat_01.png)

- :one: <u>Joindre aux entités de</u> : la couche qui va récupérer des données (*ici les axes*)

- :two: <u>les entités qu'elles (prédicat géométrique)</u>: choisir ce qui définit la relation spatiale entre les 2 couches parmi: (*ici intersecte car on veut récupérer les données des poly qui sont intersectés par les axes)*

   ![req_spatiale_01](images/req_spatiale_01.png)

- :three: <u>En comparant à</u> : la couche qui contient des données à récupérer (*ici les poly c'est à dire les faits*).

- :four: <u>Champs à ajouter</u> : Cliquer à droite sur Parcourir [...] et dans la fenêtre qui s'ouvre :five: cocher :white_large_square: numpoly  puis [OK]

   ![joindre les attributs par localisation](images\joint_spat_01b.png)

- :six: <u>Type de jointure</u> : prendre uniquement les attributs de la première entité correspondante (un à un)

- Cliquer sur le bouton [Exécuter]


 



:warning: la couche obtenue est temporaire et se nomme *Couche issue de la jointure spatiale*

## Cas 2 : obtenir des statistiques simples sur plusieurs champs

>  *Exemple :* on voudrait récupérer dans la table attributaire des axes de coupe l'altitude moyenne des points qui ont servis a les tracer .

*Traitement → Boite à outils → ![icon_epsilon](images/icon_epsilon.png) Joindre les attributs par localisation (résumé)*

:information_desk_person: Dans la boite à outils de traitement, utiliser la barre de recherche :mag_right: pour trouver l'outil *Joindre les attributs par localisation (résumé)*

![joindre les attributs par localisation](images\joint_spat_02.png)

- :one: <u>Joindre aux entités de</u> : la couche qui va récupérer des données (*ici les axes*)

- :two: <u>les entités qu'elles (prédicat géométrique)</u>: choisir ce qui définit la relation spatiale entre les 2 couches parmi: (*ici intersecte car on veut récupérer les données des points qui sont intersectés par les axes)*

- :three: <u>En comparant à</u> : la couche qui contient des données à récupérer (*ici les points c'est à dire les faits*).

- :four:<u>Champs à résumer</u> :  Cliquer à droite sur Parcourir [...] et dans la fenêtre qui s'ouvre :five: cocher :white_large_square: *Z*  (*le champ avec l'information d'altitude*) puis [OK]

  ![joindre les attributs par localisation - résumé](images\joint_spat_02b.png)

- :six: <u>Résumés à calculer</u> :  Cliquer à droite sur Parcourir [...] et dans la fenêtre qui s'ouvre :seven:choisir les fonctions statistiques (ici uniquement la moyenne, *mean* en anglais)

  ![joindre les attributs par localisation - résumé](images\joint_spat_02c.png)

- Cliquer sur le bouton [Exécuter]



:warning: la couche obtenue est temporaire et se nomme *Couche issue de la jointure spatiale* 

