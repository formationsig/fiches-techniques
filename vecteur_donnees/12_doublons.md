![](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![](images/CC.png) 

[Inrap](https://www.inrap.fr/), réseau des référents SIG - Bertrand Houdusse (2024)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.34 de QGIS
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).





# Trouver les doublons de numéros avec la calculatrice de champs

Lors du travail sur le plan des vestiges de l'opération, une tache incontournable consiste en la vérification de la présence de doublons de numéros (numéros de faits ou structures, numéros de tranchées, d'isolats, etc...).

Il est notamment <u>**primordial**</u> de vérifier les doublons de numéro avant de se lancer dans l'étiquetage d'une couche, voire avant de commencer la vectorisation des minutes.

Il existe une solution simple qui consiste à créer un champ (colonne) qui comptabilise les occurrences de chaque numéro présent dans un champ. Pour créer ce champ, on utilise une fonction native présente parmi les expressions de Qgis.

> Pour l'exemple, on utilisera une couche de faits archéologiques, dont les champs sont formatés pour correspondre au modèle des 6 couches. Mais la formule appliquée peut s'appliquer à n'importe quelle couche (ouvertures, isolats, US, points topo ou autres) et à n'importe quel champ pouvant contenir des doublons.

## Ouverture de la table attributaire

Dans l'exemple proposé, la couche concernée est celle des faits d'une opération (couche poly), au sein de laquelle on veut vérifier la présence éventuelle de doublons de numéros de faits ; ces numéros sont enregistrés dans le champ "numpoly".

![01_table_attributaire](images/doublons/01_table.jpg)

On peut donc procéder à l'ajout d'un champ, en passant par la calculatrice de champs (bouton entouré de rouge).



## Création du champ "doublon"

La manipulation principale consiste à créer un nouveau champ.

![02_calculatrice](images/doublons/02_calculatrice.jpg)

À l'ouverture de la fenêtre de la calculatrice de champs, on doit donc, dans l'ordre :

- cocher le bouton "Créer un nouveau champ"
- saisir 'doublon' dans la case en face de "Nom"
- laisser le type de champ à Entier (par défaut), car il s'agit d'un nombre
- on peut raccourcir la longueur du champ (ici, 5, au lieu de 10 par défaut)
- dans la grande case blanche sous l'onglet Expression, il faut saisir la formule suivante:
  - **`count ( 1,"numpoly" )`**
    - soit littéralement : count  	parenthèse ouvrante  	1 	 virgule 	guillemet double 	numpoly 	guillemet double 	parenthèse fermante
    - pour éviter les erreurs de saisie, on peut rechercher la fonction **count** dans le volet central, soit en saisissant les premiers caractères dans la barre de recherche, soit en cherchant la fonction dans les sous-menu (*Agrégats*), puis en double-cliquant sur le nom **count**

Cette formule va compter le nombre de fois où la chaque valeur de "numpoly" apparaît dans la table concernée.

Une fois que les paramètres sont correctement saisis, on peut cliquer sur OK. Le temps de calcul peut-être long, il faut donc attendre.



## Traitement des "doublons"

Une fois le calcul effectué, un champ "doublon" a bien été ajouté à la table attributaire. Il contient des valeurs qui correspondent en majorité à 1 (le numéro étudié n'apparait qu'une fois).  **Les doublons seront donc les lignes qui contiennent la valeur 2 (ou plus).**

![table_champ_doublon](images/doublons/03_table_doublons.jpg)

Pour examiner facilement dans la table, il est facile de trier sur l'ordre d'affichage des lignes, en cliquant deux fois sur le nom de champ (=l'en-tête) doublon : les lignes seront affichées dans l'ordre décroissant de la valeur de doublon (du plus grand au plus petit).

Par la suite, l'examen doit se poursuivre "à la main":

l'idéal est de sélectionner les lignes qui ont un doublon de numéro (on sélectionne une ligne en cliquant sur le numéro d'index à gauche du tableau, ici 1 et 2 par exemple) ; ensuite, on clique sur le bouton "zoomer sur les entités sélectionner", qui permet d'observer directement sur le plan les tracés concernés.

![doublon_selection](images/doublons/04_table_selection_zoom.jpg)

Ensuite, la correction dépend du cas de figure :

- les doublons "légitimes ": par exemple, deux tronçons de fossés disjoints mais alignés, à qui on a attribué le même numéro (par ex. pour une ornière discontinue) : on peut éventuellement fusionner les entités (ce qui crée un polygone multipartite) ou bien laisser tel quel.
- les vrais doublons :
  - deux tracés de deux structures différents portent le même numéro  : il faut vérifier à l'aide des points topographiques ou de l'inventaire des faits lequel est le bon (possible inversion de chiffres lors du marquage, du levé ou du remplissage de la table attributaire: 308 et 380 qui devient 308)
  - plusieurs tracés pour une seule structure => un vestige a été topographié plusieurs fois : il faut éliminer les tracés inutiles.

Les erreurs sont assez variées et ne saurient donc toutes être vues en détail.



À la fin du tri et du remplissage de la table, on peut relancer le calcul  avec la calculatrice de champ (cette fois-ci en ***mettant à jour*** le champ doublon)  pour vérifier qu'aucune erreur n'a été oubliée, mais surtout que la correction n'a pas créé de nouveaux doublons !!



> Notes :
>
> - le champ créé à l'aide de la procédure ci-dessus ne se met pas à jour automatiquement. Pour pallier cet inconvénient, on peut créer ce champ doublon sous la forme d'un [champ virtuel](https://docs.qgis.org/3.34/fr/docs/user_manual/working_with_vector/attribute_table.html#creating-a-virtual-field) (dont la valeur se met à jour à chaque ouverture de la table attributaire). Mais **<u>ATTENTION</u>** : les tests réalisés jusqu'à présent indiquent que cette solution peut amener à des erreurs voire à corrompre les données
> - la formule utilisée `count(1,numpoly)` peut être utilisée dans une autre couche pour vérifier les doublons d'un autre champ. Il suffit pour cela de remplacer **numpoly** par le nom du champ que l'on cherche à inspecter.

