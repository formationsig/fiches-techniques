![logo inrap](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![](images/CC.png)  

[Inrap](https://www.inrap.fr/) - équipe Formateurs & Référents SIG (2024)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.28 de QGIS
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).
>
> Ce document a  été rédigé à partir du tutoriel de [geoservices.ign.fr](https://geoservices.ign.fr/documentation/services/utilisation-sig/tutoriel-qgis/tms) et de la veille  technique  de Bertrand Houdusse  ;)



# Ajouter un flux de tuiles vectorielles (TMS)

{% hint style='info' %} L'accès à certains flux de,l'IGN  n'est plus limité au réseau Inrap,  ils sont donc utilisables sans être connecté à celui-ci  ! {% endhint %}

Ce  tutoriel  propose une alternative  au flux WMS:  utiliser les tuiles vectorielles. 

Les  tuiles vectorielles sont des images vectorielles, précalculées et sans table attributaire.  Elles se chargent  comme les flux WMS (avec un des adresses  web ou plus simplement avec un fichier xml).L'avantage par rapport aux autres flux vecteurs (WFS) c'est que  c'est plus  léger  donc  plus fluide. L'avantage par rapport à  un flux raster  c'est que l'on peut modifier le style - symbologies et étiquettes (que l'on pourra conserver  en l'enregistrant sur son propre ordi)...bref, ça fait bien le boulot. 



{% hint style='info' %}  Il existe cependant quelques inconvénients mineurs :

\- Certaines polices utilisées peuvent  ne pas existé sur l'ordinateur → Elles  sont normalement téléchargées par Qgis, si le réglage est coché dans les Options (Préférences>Options>Polices>Télécharger automatiquement les polices manquantes) ; sinon elles sont remplacées.

\- Quelques autres problèmes de rendu sont [listés sur la page de l'IGN](https://geoservices.ign.fr/documentation/services/utilisation-sig/tutoriel-qgis/tms#44466).

\- Pour certaines couches, le rendu se généralise assez vite une fois que l'on dézoome.

\- Parfois il y a trop d'étiquettes affichées → à régler au cas par cas dans les propriétés d'étiquettes.

\- Il ne faut pas ouvrir le résultat dans Adobe Illustrator, c'est inexploitable ! {% endhint %}



{% hint style='danger' %}Pour rappel, les données de l'IGN sont utilisables pour un usage scientifique, technique et pédagogique. On n'oublie pas de citer ses sources (c'est pas parce que c'est gratuit qu'on peut s'en dispenser) !{% endhint %}



## 1. Configurer une connexion TMS à l'aide du fichier xml

- Télécharger et eregistrer les fichiers xml sur votre ordinateur (fichier accessible ici  [IGN_tuiles-vectorielles_corrigees_2024_04.xml](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/fichiers/IGN_tuiles-vectorielles_corrigees_2024_04.xml?inline=false).



- Dans QGIS, ouvrir le  ![11_gestion_couche](images/11_gestion_couche.png) gestionnaire des sources de données.

- Dans le panneau Gestionnaire de sources de données:
  1) cliquer sur l'onglet  [Tuile vectorielle]
  2) cliquer sur le bouton [Charger] pour aller chercher le fichier .xml  précédemment téléchargé.
  3) Dans la fenêtre "Gérer les connexions" cliquer sur le bouton [Tout sélectionner]
  4) puis cliquer sur le bouton [Importer]

![11_charger_flux_xml](images/11_charger_flux_xml.png)



{% hint style='info' %} Attention ! Le fichier xml est une aide à la création des connexions. Il propose une sélection des ressources IGN existantes au format de tuiles vectorielles. Il existe d'autres pourvoyeurs  de flux TMS à vous de trouver ! {% endhint %}

## 2. Ajouter une couche du serveur TMS

- Pour ajouter une couche, ouvrir le  ![11_gestion_couche](images/11_gestion_couche.png) gestionnaire des sources de données. ou Ctrl + Maj + W

- Dans le menu déroulant, choisir le serveur puis cliquer sur [Connexion]

![11_charger_couche_tms](images/11_charger_couche_tms.png)

- La liste des couches disponibles s'affiche 

![11_charger_flux_xml_explorateur](images/11_charger_flux_xml_explorateur.png)

- Une fois la couche sélectionnée, cliquer sur [Ajouter], puis [Fermer].



{% hint style='danger' %}  N'oubliez pas de noter le copyright des données. Exemple : SCAN25® © IGN 2011 {% endhint %}
