![logo inrap](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![](images/CC.png)  

[Inrap](https://www.inrap.fr/) - équipe Formateurs & Référents SIG (2021)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS.
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).



# Géoréférencer des images

## 1. Manipulations

Avec l'extension ![icon_georef](images/icon_georef.png) Géoréférenceur GDAL (éventuellement à [activer dans les Extensions](https://formationsig.gitlab.io/fiches-techniques/priseenmain/05_extensions.html))

Avant tout géoréférencement, on doit choisir la couche de référence dans son projet QGIS. 

Si la couche de référence est une couche vecteur, il faut alors activer l’accrochage d’objets via la [barre d'outils d'accrochage](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/02_accrochage.html) :

![barre d'outils d'accrochage](images/georef_01.jpg)

Menu *Couche > Géoréférencer* (avant la version 3.26, le chemin était : Menu *Raster* > *Géoréférencer*)

Depuis la fenêtre du Géoréférenceur, ouvrir le raster à géoréréfencer. 

![ajouter des points de contrôle](images/georef_02.png)



- Ajouter un nouveau point de contrôle en cliquant sur le raster à géoréférencer
- Choisir «Depuis le canevas de la carte»
- Cliquer sur le point d’amer dans le canevas.
Refaire la manipulation autant de fois que l’on veut mettre de points
- Lancer le géoréférencement
- Vérifier le SCR (2154 !)
- Sauvegarder le fichier en modifiant le suffixe *_modifié* par  *_georef*
- Choisir la compression DEFLATE
- Cocher :white_large_square: employer 0 pour la transparence
- Cocher :white_large_square: ​Charger dans QGIS lorsque terminé
- [OK]

![paramètres de transformation](images/georef_03.png)



## 2. Rappel sur les transformations à utiliser

* **Helmert** : mise à l’échelle, translation et rotation (2 pts minimum)
* **Polynomiale de degré 1** : mise à l’échelle, translation et rotation (4 pts minimum)
* **Polynomiale de degré 2** : déformation du raster, distorsion «courbe» (6 pts minimum)
* **Polynomiale de degré 3** : déformation du raster (10 pts minimum)
* **Projective** : déformation du raster (à utiliser lorsque la photographie présente un angle de fuite, 4 pts minimum)

