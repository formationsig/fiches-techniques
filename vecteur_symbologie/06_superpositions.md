![logo inrap](images/inrap.png) 

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/deed.fr) ![](images/CC.png)  

[Inrap](https://www.inrap.fr/) - équipe Formateurs & Référents SIG (2021)

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS.
>
> Pour toute question sur le contenu de cette fiche technique ou pour suggérer des corrections, contactez les auteurs ou postez un sujet sur le [réseau SIG de l'intranet](https://intranet.inrap.fr/reseaux/sig).



# Gérer simplement les superpositions de vestiges

## Option 1 : Simple couper-coller

Lorsque vous dessinez dans QGIS, chaque nouvelle entité sera positionnée sur la précédente. En cas de superposition, la plus récente sera la plus visible. Avec seulement quelques superpositions, il vous suffit de couper celle qui est la plus basse dans la stratigraphie puis le la coller ; après enregistrement des modifications de la couche, elle sera bien positionnée au-dessus des autres. 



## Option 2 : via la table attributaire

### 1. Créer un champ pour la gestion

- dans la couche des vestiges, créer un champ *ordre* (type nombre entier, longueur 1, valeur pour tout le monde "0")
- dessiner les nouvelles entités et leur attribuer la valeur "-1", "0" ou "1" en fonction des superpositions selon l'ordre : -1 en dessous, puis 0, puis 1



### 2. Adapter la symbologie

- Dans les propriétés de la couche, onglet *Symbologie - Rendu de couche*, cocher *Contrôle de l'ordre de rendu des entités*, puis cliquer sur l'icône à droite pour ouvrir la boîte de dialogue.

![rendu de couche](images/superpositions_01.png) 



<img src="images/superpositions_02.png" alt="symbole à cliquer" style="zoom:150%;" />

- Sélectionner le champ *ordre*, puis choisir *Ascendant*.

![ordre de rendu](images/superpositions_03.png)



